<?php

/**
 * @file
 * Webform block region module file.
 */

/**
 * Implmenets hook_schema_alter().
 *
 * Adds the additional configuration fields to the webform configuration.
 */
function webform_ban_location_schema_alter(&$schema) {
  if (isset($schema['webform'])) {
    $schema['webform']['fields']['region_block'] = array(
      'type' => 'int',
      'description' => 'Boolean value for whether this form should be blocked by region.',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    );
    $schema['webform']['fields']['country_code'] = array(
      'type' => 'text',
      'description' => 'Country',
      'size' => 'normal',
    );
    $schema['webform']['fields']['city'] = array(
      'type' => 'text',
      'description' => 'City',
      'size' => 'normal',
    );
    $schema['webform']['fields']['zip'] = array(
      'type' => 'text',
      'description' => 'Zip',
      'size' => 'normal',
    );
    $schema['webform']['fields']['time_zone'] = array(
      'type' => 'text',
      'description' => 'Time Zone',
      'size' => 'normal',
    );
    $schema['webform']['fields']['ip_address'] = array(
      'type' => 'text',
      'description' => 'IP Address',
      'size' => 'normal',
    );
  }
}

/**
 * Implements hook_form_alter().
 *
 * Replaces webform's submit function.
 *
 * @see webform_ban_location_submit()
 */
function webform_ban_location_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'webform_configure_form') {
    $form['submission']['region_block'] = array(
      '#type' => 'checkbox',
      '#title' => 'Block users by geographical location',
      '#default_value' => $form['#node']->webform['region_block'],
      '#description' => 'If enabled submissions will be blocked per configuration settings.',
    );

    $form['submission']['ban_location_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ban Location Options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#states' => array(
        'invisible' => array(
          ':input[name="region_block"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['submission']['ban_location_options']['country_code'] = array(
      '#type' => 'select',
      '#title' => 'Country',
      '#default_value' => unserialize($form['#node']->webform['country_code']),
      '#multiple' => TRUE,
      '#options' => webform_ban_location_country_list(),
    );

    /*
    $form['submission']['ban_location_options']['region'] = array(
      '#type' => 'textfield',
      '#title' => 'Region',
      '#default_value' => $form['#node']->webform['region'],
    );
    */

    $form['submission']['ban_location_options']['city'] = array(
      '#type' => 'textfield',
      '#title' => 'City',
      '#default_value' => $form['#node']->webform['city'],
    );

    $form['submission']['ban_location_options']['zip'] = array(
      '#type' => 'textfield',
      '#title' => 'Zip',
      '#default_value' => $form['#node']->webform['zip'],
    );

    // Prepare timezone options.
    $time_zones = array();
    foreach (system_time_zones() as $key => $tz) {
      $dateTime = new DateTime();
      $dateTime->setTimeZone(new DateTimeZone($key));
      $time_zones[$dateTime->format('P')] = $tz;
    }
    ksort($time_zones);

    $form['submission']['ban_location_options']['time_zone'] = array(
      '#type' => 'select',
      '#title' => 'Time Zone',
      '#default_value' => unserialize($form['#node']->webform['time_zone']),
      '#multiple' => TRUE,
      '#options' => $time_zones,
    );

    $form['submission']['ban_location_options']['ip_address'] = array(
      '#type' => 'textfield',
      '#title' => 'IP Address',
      '#default_value' => $form['#node']->webform['ip_address'],
    );

    array_unshift($form['#submit'], 'webform_ban_location_conf_save');
  }

  if (webform_ban_location_is_enabled($form) && webform_ban_location_is_blocked($form)) {
    array_pop($form['#submit']);
    $form['#submit'][] = 'webform_ban_location_webform_submit';
  }
}

/**
 * Form submission handler for webform_client_form().
 *
 * @see webform_ban_location_form_alter()
 */
function webform_ban_location_webform_submit($form, &$form_state) {
  $form['#submit'] = array('webform_client_form_pages');

  $node = $form['#node'];

  $message = strlen(trim(strip_tags($node->webform['confirmation']))) > 0;

  // Check confirmation and redirect_url fields.
  $redirect = NULL;
  $redirect_url = trim($node->webform['redirect_url']);

  if (!empty($node->webform['confirmation_block'])) {
    $message = FALSE;
    // Webform was submitted in a block and the confirmation message is to be
    // displayed in the block.
    $_SESSION['webform_confirmation'][$node->nid] = array(
      'sid' => $sid,
      'confirmation_page' => $redirect_url == '<confirmation>',
    );
    drupal_page_is_cacheable(FALSE);
  }
  elseif ($redirect_url == '<none>') {
    // No redirect needed. Show a confirmatin message if there is one.
  }
  elseif ($redirect_url == '<confirmation>') {
    // No confirmation message needed because it will be shown on the
    // confirmation page.
    $message = FALSE;
    $redirect = array('node/' . $node->nid . '/done');
  }

  // Show a message if manually set.
  if (is_string($message)) {
    drupal_set_message($message);
  }
  // If redirecting and we have a confirmation message, show it as a message.
  elseif ($message) {
    drupal_set_message(webform_replace_tokens($node->webform['confirmation'], $node, $submission, NULL, $node->webform['confirmation_format']));
  }

  $form_state['redirect'] = $redirect;

  // Log failed attempt.
  watchdog('webform_ban_location', "Block submission: " . json_encode($form_state['input']['submitted']), $variables = array());
}

/**
 * Form submission handler for webform_configure_form().
 *
 * @see webform_ban_location_form_alter()
 */
function webform_ban_location_conf_save($form, &$form_state) {
  $form['#node']->webform['region_block'] = $form_state['values']['region_block'];
  $form['#node']->webform['country_code'] = serialize($form_state['values']['country_code']);
  $form['#node']->webform['city'] = $form_state['values']['city'];
  $form['#node']->webform['zip'] = $form_state['values']['zip'];
  $form['#node']->webform['time_zone'] = serialize($form_state['values']['time_zone']);
  $form['#node']->webform['ip_address'] = $form_state['values']['ip_address'];
}

/**
 * Determine if blocking is enabled for the given webform.
 *
 * @param string $form
 *   The webform to check.
 *
 * @return bool
 *   Whether or not the webform has blocking enabled.
 */
function webform_ban_location_is_enabled($form) {
  // Determine if the form is a webform.
  if (array_key_exists('#node', $form)
    && in_array($form['#node']->type, webform_node_types())
    && strpos($form['#form_id'], 'webform_client_form_') === 0) {
    return $form['#node']->webform['region_block'];
  }

  return FALSE;
}

/**
 * Determine if user is blocked.
 *
 * @return bool
 *   Whether or not user is blocked.
 */
function webform_ban_location_is_blocked($form) {
  $location =& $_SESSION['smart_ip']['location'];

  // If for some reason the location session values isn't set, return early.
  if (empty($location)) {
    return FALSE;
  }

  $webform =& $form['#node']->webform;

  // Unserialize fields with multiple values.
  $unserialize = array('country_code', 'time_zone');
  foreach ($unserialize as $key) {
    $webform[$key] = unserialize($webform[$key]);
  }

  // Separate out only keys that are needed for comparison.
  $user_values = array_intersect_key($location, $webform);

  // Determine if user is blocked.
  $blocked = FALSE;
  array_walk($user_values, function(&$val, $key) use ($webform, &$blocked) {
    // Convert empty strings to arrays for in_array check.
    $config = $webform[$key] == '' ? array() : $webform[$key];

    if (!$blocked && !empty($config)) {
      $blocked = in_array($val, $config);
    }
  });

  return $blocked;
}

/**
 * Returns a list of countries keyed by country code.
 *
 * @return array
 *   An array of countries keyed by country code.
 */
function webform_ban_location_country_list() {
  return array(
    'AF' => "Afghanistan",
    'AX' => "Åland Islands",
    'AL' => "Albania",
    'DZ' => "Algeria",
    'AS' => "American Samoa",
    'AD' => "Andorra",
    'AO' => "Angola",
    'AI' => "Anguilla",
    'AQ' => "Antarctica",
    'AG' => "Antigua and Barbuda",
    'AR' => "Argentina",
    'AM' => "Armenia",
    'AW' => "Aruba",
    'AU' => "Australia",
    'AT' => "Austria",
    'AZ' => "Azerbaijan",
    'BS' => "Bahamas (the)",
    'BH' => "Bahrain",
    'BD' => "Bangladesh",
    'BB' => "Barbados",
    'BY' => "Belarus",
    'BE' => "Belgium",
    'BZ' => "Belize",
    'BJ' => "Benin",
    'BM' => "Bermuda",
    'BT' => "Bhutan",
    'BO' => "Bolivia (Plurinational State of)",
    'BQ' => "Bonaire, Sint Eustatius and Saba",
    'BA' => "Bosnia and Herzegovina",
    'BW' => "Botswana",
    'BV' => "Bouvet Island",
    'BR' => "Brazil",
    'IO' => "British Indian Ocean Territory (the)",
    'BN' => "Brunei Darussalam",
    'BG' => "Bulgaria",
    'BF' => "Burkina Faso",
    'BI' => "Burundi",
    'CV' => "Cabo Verde",
    'KH' => "Cambodia",
    'CM' => "Cameroon",
    'CA' => "Canada",
    'KY' => "Cayman Islands (the)",
    'CF' => "Central African Republic (the)",
    'TD' => "Chad",
    'CL' => "Chile",
    'CN' => "China",
    'CX' => "Christmas Island",
    'CC' => "Cocos (Keeling) Islands (the)",
    'CO' => "Colombia",
    'KM' => "Comoros (the)",
    'CD' => "Congo (the Democratic Republic of the)",
    'CG' => "Congo (the)",
    'CK' => "Cook Islands (the)",
    'CR' => "Costa Rica",
    'CI' => "Côte d'Ivoire",
    'HR' => "Croatia",
    'CU' => "Cuba",
    'CW' => "Curaçao",
    'CY' => "Cyprus",
    'CZ' => "Czech Republic (the)",
    'DK' => "Denmark",
    'DJ' => "Djibouti",
    'DM' => "Dominica",
    'DO' => "Dominican Republic (the)",
    'EC' => "Ecuador",
    'EG' => "Egypt",
    'SV' => "El Salvador",
    'GQ' => "Equatorial Guinea",
    'ER' => "Eritrea",
    'EE' => "Estonia",
    'ET' => "Ethiopia",
    'FK' => "Falkland Islands (the) [Malvinas]",
    'FO' => "Faroe Islands (the)",
    'FJ' => "Fiji",
    'FI' => "Finland",
    'FR' => "France",
    'GF' => "French Guiana",
    'PF' => "French Polynesia",
    'TF' => "French Southern Territories (the)",
    'GA' => "Gabon",
    'GM' => "Gambia (the)",
    'GE' => "Georgia",
    'DE' => "Germany",
    'GH' => "Ghana",
    'GI' => "Gibraltar",
    'GR' => "Greece",
    'GL' => "Greenland",
    'GD' => "Grenada",
    'GP' => "Guadeloupe",
    'GU' => "Guam",
    'GT' => "Guatemala",
    'GG' => "Guernsey",
    'GN' => "Guinea",
    'GW' => "Guinea-Bissau",
    'GY' => "Guyana",
    'HT' => "Haiti",
    'HM' => "Heard Island and McDonald Islands",
    'VA' => "Holy See (the)",
    'HN' => "Honduras",
    'HK' => "Hong Kong",
    'HU' => "Hungary",
    'IS' => "Iceland",
    'IN' => "India",
    'ID' => "Indonesia",
    'IR' => "Iran (Islamic Republic of)",
    'IQ' => "Iraq",
    'IE' => "Ireland",
    'IM' => "Isle of Man",
    'IL' => "Israel",
    'IT' => "Italy",
    'JM' => "Jamaica",
    'JP' => "Japan",
    'JE' => "Jersey",
    'JO' => "Jordan",
    'KZ' => "Kazakhstan",
    'KE' => "Kenya",
    'KI' => "Kiribati",
    'KP' => "Korea (the Democratic People's Republic of)",
    'KR' => "Korea (the Republic of)",
    'KW' => "Kuwait",
    'KG' => "Kyrgyzstan",
    'LA' => "Lao People's Democratic Republic (the)",
    'LV' => "Latvia",
    'LB' => "Lebanon",
    'LS' => "Lesotho",
    'LR' => "Liberia",
    'LY' => "Libya",
    'LI' => "Liechtenstein",
    'LT' => "Lithuania",
    'LU' => "Luxembourg",
    'MO' => "Macao",
    'MK' => "Macedonia (the former Yugoslav Republic of)",
    'MG' => "Madagascar",
    'MW' => "Malawi",
    'MY' => "Malaysia",
    'MV' => "Maldives",
    'ML' => "Mali",
    'MT' => "Malta",
    'MH' => "Marshall Islands (the)",
    'MQ' => "Martinique",
    'MR' => "Mauritania",
    'MU' => "Mauritius",
    'YT' => "Mayotte",
    'MX' => "Mexico",
    'FM' => "Micronesia (Federated States of)",
    'MD' => "Moldova (the Republic of)",
    'MC' => "Monaco",
    'MN' => "Mongolia",
    'ME' => "Montenegro",
    'MS' => "Montserrat",
    'MA' => "Morocco",
    'MZ' => "Mozambique",
    'MM' => "Myanmar",
    'NA' => "Namibia",
    'NR' => "Nauru",
    'NP' => "Nepal",
    'NL' => "Netherlands (the)",
    'NC' => "New Caledonia",
    'NZ' => "New Zealand",
    'NI' => "Nicaragua",
    'NE' => "Niger (the)",
    'NG' => "Nigeria",
    'NU' => "Niue",
    'NF' => "Norfolk Island",
    'MP' => "Northern Mariana Islands (the)",
    'NO' => "Norway",
    'OM' => "Oman",
    'PK' => "Pakistan",
    'PW' => "Palau",
    'PS' => "Palestine, State of",
    'PA' => "Panama",
    'PG' => "Papua New Guinea",
    'PY' => "Paraguay",
    'PE' => "Peru",
    'PH' => "Philippines (the)",
    'PN' => "Pitcairn",
    'PL' => "Poland",
    'PT' => "Portugal",
    'PR' => "Puerto Rico",
    'QA' => "Qatar",
    'RE' => "Réunion",
    'RO' => "Romania",
    'RU' => "Russian Federation (the)",
    'RW' => "Rwanda",
    'BL' => "Saint Barthélemy",
    'SH' => "Saint Helena, Ascension and Tristan da Cunha",
    'KN' => "Saint Kitts and Nevis",
    'LC' => "Saint Lucia",
    'MF' => "Saint Martin (French part)",
    'PM' => "Saint Pierre and Miquelon",
    'VC' => "Saint Vincent and the Grenadines",
    'WS' => "Samoa",
    'SM' => "San Marino",
    'ST' => "Sao Tome and Principe",
    'SA' => "Saudi Arabia",
    'SN' => "Senegal",
    'RS' => "Serbia",
    'SC' => "Seychelles",
    'SL' => "Sierra Leone",
    'SG' => "Singapore",
    'SX' => "Sint Maarten (Dutch part)",
    'SK' => "Slovakia",
    'SI' => "Slovenia",
    'SB' => "Solomon Islands",
    'SO' => "Somalia",
    'ZA' => "South Africa",
    'GS' => "South Georgia and the South Sandwich Islands",
    'SS' => "South Sudan",
    'ES' => "Spain",
    'LK' => "Sri Lanka",
    'SD' => "Sudan (the)",
    'SR' => "Suriname",
    'SJ' => "Svalbard and Jan Mayen",
    'SZ' => "Swaziland",
    'SE' => "Sweden",
    'CH' => "Switzerland",
    'SY' => "Syrian Arab Republic",
    'TW' => "Taiwan (Province of China)",
    'TJ' => "Tajikistan",
    'TZ' => "Tanzania, United Republic of",
    'TH' => "Thailand",
    'TL' => "Timor-Leste",
    'TG' => "Togo",
    'TK' => "Tokelau",
    'TO' => "Tonga",
    'TT' => "Trinidad and Tobago",
    'TN' => "Tunisia",
    'TR' => "Turkey",
    'TM' => "Turkmenistan",
    'TC' => "Turks and Caicos Islands (the)",
    'TV' => "Tuvalu",
    'UG' => "Uganda",
    'UA' => "Ukraine",
    'AE' => "United Arab Emirates (the)",
    'GB' => "United Kingdom of Great Britain and Northern Ireland (the)",
    'UM' => "United States Minor Outlying Islands (the)",
    'US' => "United States of America (the)",
    'UY' => "Uruguay",
    'UZ' => "Uzbekistan",
    'VU' => "Vanuatu",
    'VE' => "Venezuela (Bolivarian Republic of)",
    'VN' => "Viet Nam",
    'VG' => "Virgin Islands (British)",
    'VI' => "Virgin Islands (U.S.)",
    'WF' => "Wallis and Futuna",
    'EH' => "Western Sahara*",
    'YE' => "Yemen",
    'ZM' => "Zambia",
    'ZW' => "Zimbabwe",
  );
}
